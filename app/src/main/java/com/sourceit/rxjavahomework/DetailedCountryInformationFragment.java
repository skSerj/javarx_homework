package com.sourceit.rxjavahomework;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.rxjavahomework.databinding.FragmentDetailedCoutryInformationBinding;
import com.sourceit.rxjavahomework.network.model.Country;

public class DetailedCountryInformationFragment extends Fragment {

    private static final String NAME = "name";
    private static final String ALPHA_2_CODE = "alpha2Code";
    private static final String ALPHA_3_CODE = "alpha3Code";
    private static final String CAPITAL = "capital";
    private static final String REGION = "region";
    private static final String SUBREGION = "subregion";
    private static final String POPULATION = "population";
    private static final String DEMONYM = "denonym";
    private static final String AREA = "area";
    private static final String GINI = "gini";
    private static final String NATIVE_NAME = "native_name";
    private static final String NUMERIC_CODE = "numeric_code";
    private static final String CIOC = "cioc";


    // TODO: Rename and change types of parameters
    private String name;
    private String alpha2Code;
    private String alpha3Code;
    private String capital;
    private String region;
    private String subregion;
    private Integer population;
    private String demonym;
    private Double area;
    private Double gini;
    private String nativeName;
    private String numericCode;
    private String cioc;

    FragmentDetailedCoutryInformationBinding binding;

    // TODO: Rename and change types and number of parameters
    public static DetailedCountryInformationFragment newInstance(Country country) {
        DetailedCountryInformationFragment fragment = new DetailedCountryInformationFragment();
        Bundle args = new Bundle();
        args.putString(NAME, country.name);
        args.putString(ALPHA_2_CODE, country.alpha2Code);
        args.putString(ALPHA_3_CODE, country.alpha3Code);
        args.putString(CAPITAL, country.capital);
        args.putString(REGION, country.region);
        args.putString(SUBREGION, country.subregion);
        args.putInt(POPULATION, country.population);
        args.putString(DEMONYM, country.demonym);
        args.putDouble(AREA, country.area);
        args.putString(GINI, String.valueOf(country.gini));
        args.putString(NATIVE_NAME, country.nativeName);
        args.putString(NUMERIC_CODE, country.numericCode);
        args.putString(CIOC, country.cioc);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            alpha2Code = getArguments().getString(ALPHA_2_CODE);
            alpha3Code = getArguments().getString(ALPHA_3_CODE);
            capital = getArguments().getString(CAPITAL);
            region = getArguments().getString(REGION);
            subregion = getArguments().getString(SUBREGION);
            population = getArguments().getInt(POPULATION);
            demonym = getArguments().getString(DEMONYM);
            area = getArguments().getDouble(AREA);
            gini = getArguments().getDouble(GINI);
            nativeName = getArguments().getString(NATIVE_NAME);
            numericCode = getArguments().getString(NUMERIC_CODE);
            cioc = getArguments().getString(CIOC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailedCoutryInformationBinding.inflate(getLayoutInflater(), container, false);
        View view = binding.getRoot();

        binding.detailedName.setText(String.format("Name: %s", name));
        binding.detailedAlpha2Code.setText(String.format("alpha2Code: %s", alpha2Code));
        binding.detailedAlpha3Code.setText(String.format("alpha3Code: %s", alpha3Code));
        binding.detailedCapital.setText(String.format("capital: %s", capital));
        binding.detailedRegion.setText(String.format("region: %s", region));
        binding.detailedSubregion.setText(String.format("subregion: %s", subregion));
        binding.detailedPopulation.setText(String.format("population: %s", population));
        binding.detailedDemonym.setText(String.format("demonym: %s", demonym));
        binding.detailedArea.setText(String.format("area: %s", area));
        binding.detailedGini.setText(String.format("gini: %s", gini));
        binding.detailedNativeName.setText(String.format("native name : %s", nativeName));
        binding.detailedNumericCode.setText(String.format("numeric code: %s", numericCode));
        binding.detailedCioc.setText(String.format("cioc: %s", cioc));

        return view;
    }
}
