package com.sourceit.rxjavahomework.network.model;

public class Country {
    public String name;

    public String getName() {
        return name;
    }

    public String alpha2Code;
    public String alpha3Code;
    public String capital;
    public String region;
    public String subregion;
    public Integer population;
    public String demonym;
    public Double area;
    public Double gini;
    public String nativeName;
    public String numericCode;
    public String cioc;
}
