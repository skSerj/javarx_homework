package com.sourceit.rxjavahomework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.sourceit.rxjavahomework.network.model.Country;

public class MainActivity extends AppCompatActivity implements ActivityNavigation {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.container, CountryFragment.newInstance())
                .commit();

    }

    @Override
    public void showDetailedCountryInformationFragment(Country country) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, DetailedCountryInformationFragment.newInstance(country))
                .addToBackStack(null)
                .commit();
    }
}
