package com.sourceit.rxjavahomework;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sourceit.rxjavahomework.databinding.CountryListBinding;
import com.sourceit.rxjavahomework.network.ApiService;
import com.sourceit.rxjavahomework.network.model.Country;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CountryFragment extends Fragment implements OnClickListener {

    private Disposable disposable;
    private CountryListBinding countryListBinding;
    private CountryAdapter adapter;
    ActivityNavigation navigation;
    private String finderCountryName;

    public static CountryFragment newInstance() {
        return new CountryFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityNavigation) {
            navigation = (ActivityNavigation) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getName() + " not implemented ActivityNavigation");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        countryListBinding = CountryListBinding.inflate(getLayoutInflater(), container, false);
        View view = countryListBinding.getRoot();
        countryListBinding.countryRecyclerList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new CountryAdapter(view.getContext(), CountryFragment.this);
        countryListBinding.countryRecyclerList.setAdapter(adapter);
        countryListBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finderCountryName = countryListBinding.inputName.getText().toString();
                adapter.notifyDataSetChanged();
            }
        });

        disposable = (Disposable) ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showInfo, this::showError);


        return view;
    }

    private void showError(Throwable throwable) {
        Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (disposable != null) {
            disposable.dispose();
        }
    }

    private void showInfo(List<Country> countries) {
        adapter.update(countries);
    }

    @Override
    public void onItemClick(Country country) {
        navigation.showDetailedCountryInformationFragment(country);
    }

    @Override
    public String hasFilter(Country country) {
        return finderCountryName;
    }
}
