package com.sourceit.rxjavahomework;

import com.sourceit.rxjavahomework.network.model.Country;

public interface ActivityNavigation {
    void showDetailedCountryInformationFragment(Country country);
}
