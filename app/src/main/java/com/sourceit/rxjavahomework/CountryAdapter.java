package com.sourceit.rxjavahomework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sourceit.rxjavahomework.databinding.CountryListBinding;
import com.sourceit.rxjavahomework.databinding.ItemCountryBinding;
import com.sourceit.rxjavahomework.network.model.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {

    private Context context;
    private List<Country> list = new ArrayList<>();
    private OnClickListener listener;
    CountryListBinding countrybinding;

    public CountryAdapter(Context context, OnClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCountryBinding binding = ItemCountryBinding.inflate(LayoutInflater.from(context), parent, false);
        return new CountryHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
        Country country = list.get(position);
        String finderCountryName = listener.hasFilter(country);
        if (list.isEmpty()) {
            holder.bind(country);
        } else {
            for (Country finderCountry : list) {
                if (finderCountryName != null) {
                    if (finderCountry.name.contains(finderCountryName)) {
                        holder.bind(finderCountry);
                    }
                } else {
                    holder.bind(country);
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void update(List<Country> countries) {
        list.clear();
        list.addAll(countries);
        notifyDataSetChanged();
    }

    static class CountryHolder extends RecyclerView.ViewHolder {
        public ItemCountryBinding binding;
        OnClickListener listener;

        public CountryHolder(ItemCountryBinding binding, OnClickListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
        }

        public void bind(final Country country) {
            binding.countryName.setText(country.name);
            binding.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(country);
                }
            });

        }
    }
}
