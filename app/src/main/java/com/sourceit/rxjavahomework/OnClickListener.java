package com.sourceit.rxjavahomework;

import com.sourceit.rxjavahomework.network.model.Country;

public interface OnClickListener {
    void onItemClick(Country country);
    String hasFilter(Country country);
}
